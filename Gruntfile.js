module.exports = function(grunt) {

  var message = grunt.option('m') || 'commit';  // Сообщение коммита, использование:
                                                // gtunt --m="commit message goes here"

  grunt.initConfig({
   
    dploy: {                                            // Task
      stage: {                                          // Target
        host: "madmax.beget.com",                  // Your FTP host
        user: "zheselevko_dp",  // Your FTP user
        pass: "f8[#r#p!",                               // Your FTP secret-password
        exclude: ["Gruntfile.js", "package.json", "node_modules/*"], // Убираем из деплоя на ftp ненужные там файлы
        path: {
            local: "",          // The local folder that you want to upload
            remote: "/"          // Where the files from the local file will be uploaded at in your remote server
        }
      }
    },
    gitcommit: {
      task:{
        options: {
          message: message,
          noVerify: false,
          noStatus: false,
          verbose: false
        },
        files:{
          src: ['.']
        }
      }
    },
    gitpull: {
      task: {
        options: {
          verbose: true
        }
      }
    },
    gitpush: {
      task: {
        options: {
          verbose: true
        }
      }
    },
  });

  grunt.loadNpmTasks('grunt-dploy');
  grunt.loadNpmTasks('grunt-git');
  // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
  grunt.registerTask('default', ['gitcommit', 'gitpull', 'gitpush', 'dploy']);

};